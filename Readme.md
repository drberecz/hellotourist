Java2EE Business to Consumer Webapp

For an overview of development, please follow link to my team mate mr.Brown's gitlab account:
https://gitlab.com/bedokbarnabas/HelloTourist-project/-/commits/master

Technologies utilized: Java2EE, Application server, HTML5, CSS3, jQuery, Javascript EcmaScript6


As part of this project I was tasked with:

- loading of config data from external source(Web domain) at startup/deployment
- integration of VanillaJS freeware calendar app
- integration of MapQuest geolocation API
- microservices (avatar generation, currency rates) integrated
- various tasks from data modelling to UI design
- Live Chat(Basic jQuery/Ajax polling)